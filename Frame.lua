-- Author      : G3m7
-- Create Date : 6/8/2019 1:14:50 AM
-- avg dmg taken addon. Just store mobname, paperdoll dmg, avg dmg taken, estimated armor, estimated HP?

AE_CreatureTable = {}

local UnitHPTable = {}
local UnitArmorTable = {}

local Frame = CreateFrame("Frame")
Frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
Frame:RegisterEvent("PLAYER_EQUIPMENT_CHANGED")
Frame:RegisterEvent("PLAYER_TARGET_CHANGED")
Frame:RegisterEvent("UNIT_PET")
Frame:RegisterEvent("PLAYER_LOGIN")
Frame:RegisterEvent("UNIT_STATS")
Frame:RegisterEvent("UNIT_DAMAGE")
--Frame:RegisterEvent("UNIT_AURA")
local critModifier=2
playerLevel = UnitLevel("player")

local petLow, petHi, petAvg
local lowDmg, hiDmg, offlowDmg, offhiDmg = UnitDamage("player");
local swingAvgDmg = (lowDmg+hiDmg)/2
local _, rangeLowDmg, rangeHiDmg = UnitRangedDamage("player");
local rangeAvgDmg = (rangeLowDmg+rangeHiDmg)/2

local me = UnitName("player")
local petName = UnitName("pet")
local myGUID = UnitGUID("player")
local petGUID = UnitGUID("pet")

local targetGuid = nil
local huntersMarks = {
    [1130] = 20,
    [14323] = 45,
    [14324] = 75,
    [14325] = 110
}
function UpdatePlayerStats()
    lowDmg, hiDmg, offlowDmg, offhiDmg = UnitDamage("player");
    swingAvgDmg = (lowDmg+hiDmg)/2
    rangeSpeed, rangeLowDmg, rangeHiDmg = UnitRangedDamage("player");
    rangeAvgDmg = (rangeLowDmg+rangeHiDmg)/2
    for i=1, 4 do
        local spellId = select(10, UnitDebuff("target", i))
        if spellId == nil then return end
        local ap = huntersMarks[spellId]
        if ap ~= nil then
            local add = rangeSpeed * ap / 3.5
            rangeAvgDmg = rangeAvgDmg + add
            return
        end
    end
end
function UpdatePetStats()
    petGUID = UnitGUID("pet")
    petLow, petHi = UnitDamage("pet");
    petAvg = (petLow+petHi)/2
end

Frame:SetScript("OnEvent", function(self, event, arg1, arg2, arg3)
    if event == "COMBAT_LOG_EVENT_UNFILTERED" then
        self:OnEvent(CombatLogGetCurrentEventInfo())
    elseif event == "PLAYER_EQUIPMENT_CHANGED" or event == "PLAYER_LOGIN" then
        UpdatePlayerStats()
    elseif event == "PLAYER_LEVELUP" then
        playerLevel = tonumber(arg1)
    elseif event == "PLAYER_TARGET_CHANGED" then
        targetGuid = UnitGUID("target")
    elseif event == "UNIT_PET" then
        UpdatePetStats()
    elseif event == "UNIT_STATS" or event == "UNIT_DAMAGE" then
        if arg1 == "player" then
            UpdatePlayerStats()
        elseif arg1 == "pet" then
            UpdatePetStats()
        end
    end
end)

local function IsMyDamage(sourceGUID)
    if myGUID == sourceGUID or (petGUID ~= nil and petGUID == sourceGUID) then 
        return true
    else
        --GuidePrint(sourceGUID..", "..myGUID..", "..tostring(petGUID))
        return false
    end
end

local function UpdateHpTable(sourceName, sourceGUID, destGUID, destName, amount, overkill, destName, blocked)
    
    local v = amount
    if overkill > 0 then
        --print("overkill: "..overkill..", v:"..v)
        if v > overkill then
            v = v-overkill
        end
        --v = v - overkill
    end
    if blocked ~= nil and blocked > 0 then
        v = v + blocked
    end
    local hpEntry = UnitHPTable[destGUID]
    if hpEntry == nil then
        hpEntry = {n=destName,v=v,myDamageOnly=IsMyDamage(sourceGUID)}
        UnitHPTable[destGUID] = hpEntry
    else
        hpEntry.v = hpEntry.v + v
    end
    if hpEntry.l == nil and destGUID == targetGuid then
        hpEntry.l = UnitLevel("target")
    end
    local isMyDmg = IsMyDamage(sourceGUID)
    if isMyDmg == false then
        hpEntry.myDamageOnly = false
        return
    end

    --print(v.." HP: "..hpEntry.v)
end

local function UpdateArmorTable(destGUID, amount, blocked, absorbed, avgDmg, glancing)
        if critical then return end -- just making it simple and ignoring crits
        if glancing ~= nil and glancing == true then return end
        local hitDmg = amount -- hitDmg includes overkill
        if blocked then 
            hitDmg = hitDmg+blocked 
        end
        if absorbed then hitDmg = hitDmg+absorbed end
        

        local reductionPct = 1-(hitDmg/avgDmg)
        local armor = (reductionPct*(400+85*playerLevel))/(1-reductionPct)
        local armorEntry = UnitArmorTable[destGUID]
        if armorEntry == nil then
            UnitArmorTable[destGUID] = {count=1,avg=armor,raw=armor,reductionAvg=reductionPct,reductionRaw=reductionPct}
        else
            armorEntry.count = armorEntry.count + 1
            armorEntry.raw = armorEntry.raw + armor
            armorEntry.avg = armorEntry.raw / armorEntry.count
            armorEntry.reductionRaw = armorEntry.reductionRaw + reductionPct
            armorEntry.reductionAvg = armorEntry.reductionRaw / armorEntry.count

        end
end

function Frame:OnEvent(event, ...)
	local subevent, _, sourceGUID, sourceName, sourceFlags, sourceRaidFlags, destGUID, destName, destFlags, destRaidFlags = ...
    if destGUID == myGUID or (petGUID ~= nil and destGUID == petGUID) then return end

    if subevent == "UNIT_DIED" then--or subevent == "PARTY_KILL" 
        local x = UnitHPTable[destGUID]
        if x ~= nil then 
            if x.myDamageOnly == true and x.l ~= nil then
                local newEntry = UnitArmorTable[destGUID]
                if newEntry ~= nil then
                    local key = x.l.."-"..destName
                    local existing = AE_CreatureTable[key]
                    if existing == nil then
                        existing = {Npc=destName, Lvl=x.l, HpAvg=x.v, HpRaw=x.v, ArmorAvg=newEntry.avg, Count=1, ArmorRaw=newEntry.avg}
                        AE_CreatureTable[key] = existing
                    else
                        existing.Count = existing.Count+1
                        
                        existing.HpRaw = existing.HpRaw + x.v
                        existing.HpAvg =  existing.HpRaw / existing.Count

                        existing.ArmorRaw = existing.ArmorRaw + newEntry.avg
                        existing.ArmorAvg = existing.ArmorRaw / existing.Count
                    end
                    print("This mob HP: "..x.v..", Armor: "..newEntry.avg.." ".. (newEntry.reductionAvg*100).."%")
                    print(existing.Count.." recorded lvl "..x.l.." "..destName..": avgHP: "..existing.HpAvg..", avgArmor: "..existing.ArmorAvg)
                else
                    print("missing armor entry")
                end
            else 
                --print("not all your dmg")
            end
        end
    end
    
    if string.find(subevent, "_DAMAGE") == nil then return end


	if subevent == "SWING_DAMAGE" then
        if sourceGUID == myGUID then
            UpdatePlayerStats()
        elseif petGUID ~= nil and petGUID == sourceGUID then
            UpdatePetStats()
        end
        local amount, overkill, _, _, blocked, absorbed, critical, glancing, crushing, isOffHand = select(11, ...)
        UpdateHpTable(sourceName, sourceGUID, destGUID, destName, amount, overkill, destName, blocked)
        if sourceGUID == myGUID then
            UpdateArmorTable(destGUID, amount, blocked, absorbed, swingAvgDmg, glancing)
        elseif petGUID ~= nil and sourceGUID == petGUID then
            UpdateArmorTable(destGUID, amount, blocked, absorbed, petAvg, glancing)
        end
    elseif subevent == "SPELL_DAMAGE" then
        local amount, overkill, school, _, blocked, absorbed, critical, glancing, crushing, isOffHand = select(14, ...)
        UpdateHpTable(sourceName, sourceGUID, destGUID, destName, amount, overkill, destName)
    elseif subevent == "SPELL_PERIODIC_DAMAGE" then
        local amount, overkill, school, _, blocked, absorbed, critical, glancing, crushing, isOffHand = select(14, ...)
        UpdateHpTable(sourceName, sourceGUID, destGUID, destName, amount, overkill, destName)
	elseif targetGuid ~= nil and subevent == "RANGE_DAMAGE" and sourceName == me and destGUID == targetGuid then
        if sourceGUID == myGUID then
            UpdatePlayerStats()
        end
        --print(subevent..", "..tostring(sourceName)..", "..spellName..", "..amount..", "..tostring(critical)..", "..tostring(overkill)..", "..tostring(blocked)..", "..tostring(absorbed))
        local spellId, spellName, spellSchool, amount, overkill, school, _, blocked, absorbed, critical, glancing, crushing, isOffHand = select(11, ...)
        UpdateHpTable(sourceName, sourceGUID, destGUID, destName, amount, overkill, destName)
        UpdateArmorTable(destGUID, amount, blocked, absorbed, rangeAvgDmg)
    end
end
-- 01:23:40